package fo.nya.chat.command;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;

/**
 * This command closes the connection.
 *
 * @see Command
 * @see Context#disconnect()
 */
public class CommandDisconnect implements Command {

    /**
     * This command can be run without authentication.
     *
     * @return always return {@code false}
     * @see Command#isAuthenticationRequired()
     */
    @Override public boolean isAuthenticationRequired() {
        return false;
    }

    /**
     * {@inheritDoc}
     * <br>
     * <br>
     * In this case keyword is '{@code disconnect}'.
     */
    @Override public String getKeyword() {
        return "disconnect";
    }

    /**
     * Command closes the current context connection.
     *
     * @param param   Text parameter for the command. Ignored in this case.
     * @param context The context of the user invoking the command.
     * @see Context#disconnect()
     */
    @Override public void accept(String param, Context context) {
        context.accept(Message.textOnly( "Bye-bye, the connection will be closed..."));
        context.disconnect();
    }
}
