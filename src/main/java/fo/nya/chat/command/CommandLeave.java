package fo.nya.chat.command;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.service.chat.ChatService;
import lombok.RequiredArgsConstructor;

/**
 * This command is makes user to leave the current channel.
 *
 * @see Command
 * @see ChatService#leave(long)
 */
@RequiredArgsConstructor
public class CommandLeave implements Command {

    /**
     * Link to {@link ChatService}, required for internal needs.
     */
    private final ChatService chats;

    /**
     * This command requires authentication.
     *
     * @return always return {@code true}
     * @see Command#isAuthenticationRequired()
     */
    @Override public boolean isAuthenticationRequired() {
        return true;
    }

    /**
     * {@inheritDoc}
     * <br>
     * <br>
     * In this case keyword is '{@code leave}'.
     */
    @Override public String getKeyword() {
        return "leave";
    }

    /**
     * Command makes user to leave the current channel.
     *
     * @param param   Text parameter for the command. Ignored in this case.
     * @param context The context of the user invoking the command.
     * @see ChatService#leave(long)
     */
    @Override public void accept(String param, Context context) {
        chats.leave(context.getUserId());
        context.accept(Message.textOnly("You left chat."));
    }
}
