package fo.nya.chat.command;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.entity.User;
import fo.nya.chat.service.UserService;
import fo.nya.chat.service.chat.ChatService;
import lombok.RequiredArgsConstructor;

/**
 * This command authenticate user under specific name.
 * <br>
 * After successful authentication commands tries to add user into the chat where user was active last time.
 *
 * @see Command
 * @see UserService#loginOrCreateNewUser(String, String)
 * @see User#getLatestChatId()
 */
@RequiredArgsConstructor
public class CommandLogin implements Command {

    /**
     * Link to {@link UserService}, required for internal needs.
     */
    private final UserService users;

    /**
     * Link to {@link ChatService}, required for internal needs.
     */
    private final ChatService chats;

    /**
     * This command can be run without authentication.
     *
     * @return always return {@code false}
     * @see Command#isAuthenticationRequired()
     */
    @Override public boolean isAuthenticationRequired() {
        return false;
    }

    /**
     * {@inheritDoc}
     * <br>
     * <br>
     * In this case keyword is '{@code login}'.
     */
    @Override public String getKeyword() {
        return "login";
    }

    /**
     * Command authenticate user and tries to add user into {@link User#getLatestChatId()} chat.
     *
     * @param param   Username and password separated by at least one space character({@code \s+}).
     * @param context The context of the user invoking the command.
     */
    @Override public void accept(String param, Context context) {
        String[] split = param.split("\\s+", 2);

        if (split.length != 2) {
            throw new RuntimeException("Bad usage of 'login' command, correct syntax is '/login <name> <pass>'");
        }

        User user = users.loginOrCreateNewUser(split[0], split[1]);

        Long userId = user.getId();

        context.setUserId(userId);

        context.accept(Message.textOnly("You authenticated as: " + split[0]));

        chats.addContext(context);

        Long latestChatId = user.getLatestChatId();

        if (latestChatId != null) {
            context.accept(Message.textOnly("You redirected into channel: " + chats.getChatName(latestChatId)));
            chats.join(latestChatId, context.getUserId());
        } else {
            context.accept(Message.textOnly("Use command '/join <chat name>' to join some chat."));
        }

    }
}
