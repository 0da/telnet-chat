package fo.nya.chat.command;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.service.chat.ChatService;
import lombok.RequiredArgsConstructor;

import java.util.stream.Collectors;

/**
 * This command shows list of currently active chats with limit of {@link #LIMIT}.
 *
 * @see Command
 * @see ChatService#getActiveChats()
 */
@RequiredArgsConstructor
public class CommandList implements Command {

    /**
     * Limit of chat names to send.
     */
    private static final int LIMIT = 25;

    /**
     * Delimiter for chat names concatenation.
     */
    private static final String DELIMITER = ", ";

    /**
     * Link to {@link ChatService}, required for internal needs.
     */
    private final ChatService chats;

    /**
     * This command requires authentication.
     *
     * @return always return {@code true}
     * @see Command#isAuthenticationRequired()
     */
    @Override public boolean isAuthenticationRequired() {
        return true;
    }

    /**
     * {@inheritDoc}
     * <br>
     * <br>
     * In this case keyword is '{@code list}'.
     */
    @Override public String getKeyword() {
        return "list";
    }

    /**
     * Command send to user list of currently active chats, no more than {@link #LIMIT}.
     *
     * @param param   Text parameter for the command. Ignored in this case.
     * @param context The context of the user invoking the command.
     * @see ChatService#getActiveChats()
     */
    @Override public void accept(String param, Context context) {

        String collect = chats.getActiveChats().stream()
                .limit(LIMIT)
                .map(chats::getChatName)
                .collect(Collectors.joining(DELIMITER));

        context.accept(Message.textOnly("Currently active chats: " + collect));
    }
}
