package fo.nya.chat.command;

import fo.nya.chat.Context;
import fo.nya.chat.service.chat.ChatService;
import lombok.RequiredArgsConstructor;

/**
 * This command adds a user into a specific chat.
 *
 * @see Command
 * @see ChatService#join(long, long)
 */
@RequiredArgsConstructor
public class CommandJoin implements Command {

    /**
     * Link to {@link ChatService}, required for internal needs.
     */
    private final ChatService chats;

    /**
     * This command requires authentication.
     *
     * @return always return {@code true}
     * @see Command#isAuthenticationRequired()
     */
    @Override public boolean isAuthenticationRequired() {
        return true;
    }

    /**
     * {@inheritDoc}
     * <br>
     * <br>
     * In this case keyword is '{@code join}'.
     */
    @Override public String getKeyword() {
        return "join";
    }


    /**
     * Command is trying to add a user to a specific chat.
     *
     * @param param   The name of the target chat.
     * @param context The context of the user invoking the command.
     * @see Context#disconnect()
     * @see ChatService#join(long, long)
     */
    @Override public void accept(String param, Context context) {
        long chatId = chats.getOrCreateChatIdForName(param);
        chats.join(chatId, context.getUserId());
    }
}
