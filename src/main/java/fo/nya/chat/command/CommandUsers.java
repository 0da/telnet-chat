package fo.nya.chat.command;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.entity.User;
import fo.nya.chat.service.UserService;
import fo.nya.chat.service.chat.ChatService;
import lombok.RequiredArgsConstructor;

import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This command shows list of users in current chat.
 *
 * @see Command
 */
@RequiredArgsConstructor
public class CommandUsers implements Command {

    /**
     * Delimiter for usernames concatenation.
     */
    private static final String DELIMITER = ", ";

    /**
     * Link to {@link UserService}, required for internal needs.
     */
    private final UserService users;

    /**
     * Link to {@link ChatService}, required for internal needs.
     */
    private final ChatService chats;

    /**
     * This command requires authentication.
     *
     * @return always return {@code true}
     * @see Command#isAuthenticationRequired()
     */
    @Override public boolean isAuthenticationRequired() {
        return true;
    }

    /**
     * {@inheritDoc}
     * <br>
     * <br>
     * In this case keyword is '{@code users}'.
     */
    @Override public String getKeyword() {
        return "users";
    }

    /**
     * Command shows list of users in current chat.
     *
     * @param param   Text parameter for the command. Ignored in this case.
     * @param context The context of the user invoking the command.
     * @throws RuntimeException in case if user currently no in a chat.
     * @see ChatService#getActiveChats()
     */
    @Override public void accept(String param, Context context) {
        Optional<Long> chatId = chats.getChatIdForUser(context.getUserId());

        Set<Long> members = chats.getMembersOfChat(chatId.orElseThrow(
                () -> new RuntimeException("You are not in the channel. Use command '/join <chanel name>'")
        ));

        String names = members.stream()
                .map(users::getUserById)
                .map(User::getName)
                .filter(Objects::nonNull)
                .collect(Collectors.joining(DELIMITER));

        context.accept(Message.textOnly("Users in current chat: " + names));
    }
}
