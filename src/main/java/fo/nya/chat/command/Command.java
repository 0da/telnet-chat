package fo.nya.chat.command;

import fo.nya.chat.Context;

/**
 * The base interface for all 'commands' in chat.
 * <br>
 * General layout of command: {@code [special symbol][keyword] [param]}
 */
public interface Command {

    /**
     * Flag indicating that authorization is required to execute the command. In other words, the condition {@link Context#getUserId()} {@code != null} must be true.
     *
     * @return {@code true} if {@link Context#getUserId()} must be not {@code null}.
     */
    boolean isAuthenticationRequired();

    /**
     * The keyword for this command. Without technical characters, such as {@code /\!@}. For example: {@code jump}, not a {@code /jump} or {@code !jump}.
     *
     * @return keyword to invoke this command.
     */
    String getKeyword();

    /**
     * The body of the command, in it the command does everything it should.
     *
     * @param param   Text parameter for the command.
     * @param context The context of the user invoking the command.
     */
    void accept(String param, Context context);
}