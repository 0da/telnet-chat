package fo.nya.chat.net;

import fo.nya.chat.Context;
import fo.nya.chat.messages.MessageConsumer;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import lombok.RequiredArgsConstructor;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;

/**
 * Initializer for chat pipeline.
 */
@RequiredArgsConstructor
public class ChatChannelInitializer extends ChannelInitializer<Channel> {

    /**
     * Maximum size of one message in bytes.
     */
    private final static int MAX_FRAME_SIZE = 4096;

    /**
     * A delimiter to separate one message from another.
     */
    private final static ByteBuf CR_DELIMITER = Unpooled.wrappedBuffer(new byte[]{'\r'});

    /**
     * Byte to string converter for income 'messages'.
     */
    private final StringDecoder decoder = new StringDecoder(StandardCharsets.UTF_8);

    /**
     * String to byte converter for outcome 'messages'.
     */
    private final StringEncoder encoder = new StringEncoder(StandardCharsets.UTF_8);

    /**
     * 'Pipeline' of user messages consumers. MessageConsumers are called until one of them returns {@code true}.
     */
    private final List<MessageConsumer> messageConsumerPipeline;

    /**
     * 'Factory' to create a new context object based on the channel object.
     */
    private final Function<Channel, Context> contextFactory;

    /**
     * @param ch the {@link Channel} which was registered.
     */
    @Override protected void initChannel(Channel ch) {
        ch.pipeline().addLast(
                new DelimiterBasedFrameDecoder(MAX_FRAME_SIZE, CR_DELIMITER),
                decoder,
                encoder,
                new ChatHandler(contextFactory, messageConsumerPipeline)
        );
    }
}
