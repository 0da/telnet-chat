package fo.nya.chat.net;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.messages.MessageConsumer;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.function.Function;

/**
 * Object has a few uses:
 * <ul>
 *     <li>Stores the context for the current connection.</li>
 *     <li>When a new message arrives, it calls the MessageConsumer chain until first {@code true}</li>
 * </ul>
 * <p>
 * Object is stateful and must be created per connection.
 *
 * @see Context
 * @see MessageConsumer
 */
@RequiredArgsConstructor
public class ChatHandler extends SimpleChannelInboundHandler<String> {

    /**
     * 'Factory' to create a new context object based on the channel object.
     */
    private final Function<Channel, Context> contextFactory;

    /**
     * 'Pipeline' of user messages consumers. MessageConsumers are called until one of them returns {@code true}.
     */
    private final List<MessageConsumer> messageConsumersPipeline;

    /**
     * Context of a current connection.
     */
    private Context context;

    @Override public void channelRegistered(ChannelHandlerContext ctx) {
        this.context = contextFactory.apply(ctx.channel());
        this.context.accept(Message.textOnly("Welcome into simple chat."));
    }

    /**
     * When a new message arrives, it calls the MessageConsumer chain until first {@code true}.
     *
     * @see #messageConsumersPipeline
     */
    @Override protected void channelRead0(ChannelHandlerContext ctx, String msg) {

        msg = msg.trim();

        for (MessageConsumer consumer : messageConsumersPipeline) {
            if (consumer.accept(msg, context)) {
                return;
            }
        }
    }

    /**
     * The method sends an exception message to the user.
     */
    @Override public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        context.accept(Message.textOnly(cause.getMessage()));
    }
}
