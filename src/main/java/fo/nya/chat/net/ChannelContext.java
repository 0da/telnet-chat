package fo.nya.chat.net;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.util.function.LongFunction;

/**
 * Context implementation over Netty Chanel
 */
@RequiredArgsConstructor
public class ChannelContext implements Context {

    /**
     * {@code userId} to {@code username} converter. Required for proper name display in messages.
     */
    private final LongFunction<CharSequence> userName;

    /**
     * Chanel behind this context.
     */
    private final Channel channel;

    /**
     * {@code id} of a user that currently associated with this context;
     */
    @Getter
    @Setter
    private Long userId;

    /**
     * {@inheritDoc}
     * <br> Send and flush message into {@link #channel}. Message format:
     * <br> {@code [name]: message text\r\n}
     *
     * @param message that should be proceeded.
     */
    @Override public void accept(Message message) {
        Long id = message.getUserId();

        String name = id == null ? "<System>: " : ("[" + userName.apply(id) + "]: ");

        channel.writeAndFlush(name + message.getText() + "\r\n");
    }

    /**
     * {@inheritDoc}
     */
    @Override public void disconnect() {
        channel.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override public boolean isActive() {
        return channel.isOpen();
    }

}
