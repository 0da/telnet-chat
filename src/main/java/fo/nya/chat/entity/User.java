package fo.nya.chat.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity that represent single user.
 */
@NoArgsConstructor
@Data public class User {

    /**
     * Personal unique ID for that entity.
     */
    private Long id;

    /**
     * ID of chat where users was active last time.
     */
    private Long latestChatId;

    /**
     * Username.
     */
    private String name;

    /**
     * User password.
     */
    private String pass;

    /**
     * Copy constructor.
     */
    public User(User original) {
        this.id = original.id;
        this.latestChatId = original.latestChatId;
        this.name = original.name;
        this.pass = original.pass;
    }
}
