package fo.nya.chat.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entity that represent single message.
 */
@NoArgsConstructor
@Data public class Message {

    /**
     * Personal unique ID for that entity.
     */
    private Long id;

    /**
     * ID of the chat this message belongs to.
     */
    private Long chatId;

    /**
     * ID of the user this message belongs to.
     */
    private Long userId;

    /**
     * Text of this message.
     */
    private String text;

    /**
     * Creation date of this message in milliseconds.
     */
    private long created;

    /**
     * Copy constructor.
     */
    public Message(Message other) {
        this.id = other.id;
        this.chatId = other.chatId;
        this.userId = other.userId;
        this.text = other.text;
        this.created = other.created;
    }

    /**
     * Simple shortcut static constructor. Creates new message entity with empty {@link #id} and current date as 'creation date'.
     *
     * @param chatId ID of the chat this message belongs to.
     * @param userId ID of the user this message belongs to.
     * @param text   Text of this message.
     */
    public static Message of(long chatId, Long userId, String text) {
        Message message = new Message();
        message.setChatId(chatId);
        message.setUserId(userId);
        message.setText(text);
        message.setCreated(System.currentTimeMillis());
        return message;
    }

    /**
     * Simple shortcut static constructor. Creates new message with defined {@link #text} field.
     *
     * @param text Text of this message.
     */
    public static Message textOnly(String text) {
        Message message = new Message();
        message.setText(text);
        return message;
    }

}
