package fo.nya.chat;

import fo.nya.chat.command.*;
import fo.nya.chat.messages.CommandConsumer;
import fo.nya.chat.messages.MessageConsumer;
import fo.nya.chat.messages.SendConsumer;
import fo.nya.chat.net.ChannelContext;
import fo.nya.chat.net.ChatChannelInitializer;
import fo.nya.chat.repo.memory.InMemoryChatRepo;
import fo.nya.chat.repo.memory.InMemoryMessageRepo;
import fo.nya.chat.repo.memory.InMemoryUserRepo;
import fo.nya.chat.service.UserService;
import fo.nya.chat.service.chat.ChatService;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * Created by 0da on 19.04.2023 15:18; (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧
 */
public class Application {
    public static void main(String[] args) {

        // "Configs"
        //////////////////////////////////////////////////////

        final int chatUpdateThreadAmount = 3;
        final int messageBroadcastThreadAmount = 1;
        final int chatBacklogSize = 10;
        final int updateQueueSize = 42;
        final int messageQueueSize = 333;
        final int maximumAmountOfUsersPerChat = 10;

        final String commandPrefix = "/";

        final int port = 8093;
        final int connectionAmount = 500;
        final int threadForParentNettyThreadGroup = 2;
        final int threadForChildNettyThreadGroup = 4;


        // "Dependency Injection"
        //////////////////////////////////////////////////////

        InMemoryUserRepo repo = new InMemoryUserRepo();
        InMemoryChatRepo chatRepo = new InMemoryChatRepo();
        InMemoryMessageRepo messageRepo = new InMemoryMessageRepo();

        UserService users = new UserService(repo);

        ChatService chats = new ChatService(chatUpdateThreadAmount, messageBroadcastThreadAmount, chatBacklogSize, updateQueueSize, messageQueueSize, maximumAmountOfUsersPerChat, users, chatRepo, messageRepo);

        List<MessageConsumer> messageConsumersPipeline = List.of(
                new CommandConsumer(commandPrefix,
                        new CommandDisconnect(),
                        new CommandJoin(chats),
                        new CommandLeave(chats),
                        new CommandList(chats),
                        new CommandLogin(users, chats),
                        new CommandUsers(users, chats)
                ),
                new SendConsumer(chats)
        );

        Function<Channel, Context> contextFactory = ch -> new ChannelContext(userId -> users.getUserById(userId).getName(), ch);

        ChatChannelInitializer chatChannelInitializer = new ChatChannelInitializer(messageConsumersPipeline, contextFactory);

        // "Scheduling"
        //////////////////////////////////////////////////////

        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor(
                r -> new Thread(r, "Chat Cleanup Thread") {{setDaemon(true);}}
        );

        scheduler.scheduleWithFixedDelay(chats::clean, 0, 30, TimeUnit.SECONDS);

        // Server startup
        //////////////////////////////////////////////////////

        NioEventLoopGroup parent = new NioEventLoopGroup(threadForParentNettyThreadGroup);
        NioEventLoopGroup child = new NioEventLoopGroup(threadForChildNettyThreadGroup);

        try {
            try {

                ServerBootstrap bootstrap = new ServerBootstrap();

                bootstrap.group(parent, child)
                        .channel(NioServerSocketChannel.class)
                        .childHandler(chatChannelInitializer)
                        .option(ChannelOption.SO_BACKLOG, connectionAmount)
                        .childOption(ChannelOption.SO_KEEPALIVE, true);


                ChannelFuture f = bootstrap.bind(port).sync();

                f.channel().closeFuture().sync();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        } finally {
            parent.shutdownGracefully();
            child.shutdownGracefully();
        }
    }
}
