package fo.nya.chat;

import fo.nya.chat.entity.Message;

/**
 * Context of that holds user connection and authentication. Interface made for separation between network layer and logic itself.
 */
public interface Context {
    /**
     * Method returns current {@code userId} associated with this context.
     *
     * @return current {@code userId} associated with this context.
     */
    Long getUserId();

    /**
     * Method sets {@code userId} that will be associated with this context.
     */
    void setUserId(Long userId);

    /**
     * Method should accept messages from logic level and deliver them to network layer.
     *
     * @param message that should be proceeded.
     */
    void accept(Message message);

    /**
     * Method disconnects connection that lays under implementation. That mean context must become 'inactive'.
     *
     * @see #isActive()
     */
    void disconnect();

    /**
     * Method checks whatever is context still active and can accept messages.
     *
     * @return {@code true} if context still active, or else {@code false}.
     * @see #accept(Message)
     * @see #disconnect()
     */
    boolean isActive();
}
