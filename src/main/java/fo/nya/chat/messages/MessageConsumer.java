package fo.nya.chat.messages;

import fo.nya.chat.Context;

/**
 * General message consumer. Interface made for separation between network layer and logic itself.
 */
@FunctionalInterface
public interface MessageConsumer {

    /**
     * The method takes a message and does something with it... or not, as you wish.
     *
     * @param msg     text representation of message.
     * @param context The context of the user invoking the command.
     * @return {@code true} if the consumer accepts this message and has processed it, otherwise {@code false}.
     */
    boolean accept(String msg, Context context);
}
