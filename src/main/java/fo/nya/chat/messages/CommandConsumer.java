package fo.nya.chat.messages;

import fo.nya.chat.Context;
import fo.nya.chat.command.Command;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * This message consumer accepts all messages starting with {@link #commandPrefix} and treats them as commands.
 * <br>
 * General layout of command: {@code [command prefix][keyword] [param]}
 */
public class CommandConsumer implements MessageConsumer {

    /**
     * The prefix with which the command must begin.
     */
    private final String commandPrefix;

    /**
     * Collections of all known commands and their prefixed keywords respectively.
     */
    private final Map<String, Command> commands;

    /**
     * Creates a new CommandConsumer with predefined {@link #commandPrefix} and set of commands.
     *
     * @param commandPrefix the prefix with which the command must begin.
     * @param commands      list of commands for that consumer.
     * @throws RuntimeException in case if two commands have same keyword({@link Command#getKeyword()}).
     */
    public CommandConsumer(String commandPrefix, Command... commands) {

        this.commandPrefix = commandPrefix;
        HashMap<String, Command> commandsMap = new HashMap<>();

        for (Command command : commands) {
            commandsMap.merge(commandPrefix + command.getKeyword(), command, (a, b) -> {
                throw new RuntimeException("Duplicate command keyword '" + a.getKeyword() + "'.");
            });
        }

        this.commands = Collections.unmodifiableMap(commandsMap);
    }


    /**
     * The method checks that {@code msg} starts with {@link #commandPrefix}, and if so, {@code msg} is treated as a command.
     *
     * @param msg     text representation of message.
     * @param context The context of the user invoking the command.
     * @return {@code true} if message was command and processed correctly.
     * @throws RuntimeException in case if there is no command with such keyword or if command requires authentication and context is not authenticated.
     * @see Context#getUserId()
     * @see Command#isAuthenticationRequired()
     * @see Command#getKeyword()
     */
    public boolean accept(String msg, Context context) {
        // Message started with started commandPrefix treated as 'command'.
        if (!msg.startsWith(commandPrefix)) {
            return false;
        }

        String[] commandAndParam = msg.split("\\s+", 2);

        Command command = commands.get(commandAndParam[0]);

        if (command == null) {
            throw new RuntimeException("Unknown command '" + commandAndParam[0] + "'.");
        }

        if (context.getUserId() == null && command.isAuthenticationRequired()) {
            throw new RuntimeException("Authentication or Registration is required. For both use command '/login <name> <pass>'.");
        }

        command.accept(commandAndParam.length > 1 ? commandAndParam[1].trim() : "", context);

        return true;

    }
}
