package fo.nya.chat.messages;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.service.chat.ChatService;
import lombok.RequiredArgsConstructor;

/**
 * This message consumer treats all messages simple as 'text messages'.
 *
 * @see ChatService#broadcastAsync(Message)
 */
@RequiredArgsConstructor
public class SendConsumer implements MessageConsumer {

    /**
     * Link to {@link ChatService}, required for internal needs.
     */
    private final ChatService chats;

    /**
     * Sends {@code msg} 'from user' into chat where user currently belongs.
     *
     * @param msg     text representation of message.
     * @param context The context of the user invoking the command.
     * @return always {@code true}.
     * @throws RuntimeException in case if context is not authenticated or if user not in a chat.
     * @see ChatService#broadcastAsync(Message)
     */
    @Override public boolean accept(String msg, Context context) {

        Long userId = context.getUserId();

        if (userId == null) {
            throw new RuntimeException("Authentication or Registration is required. For both use command '/login <name> <pass>'.");
        }

        Long chatId = chats.getChatIdForUser(userId).orElseThrow(
                () -> new RuntimeException("You not in a chat. Use command '/join <chat name>'.")
        );

        chats.broadcastAsync(Message.of(chatId, userId, msg));

        return true;
    }
}
