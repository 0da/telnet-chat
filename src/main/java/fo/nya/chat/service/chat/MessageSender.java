package fo.nya.chat.service.chat;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

/**
 * Simple wrapper class around {@link Context} that controls math messages will be sent to user.
 */
@RequiredArgsConstructor
class MessageSender {

    /**
     * Biggest message id that was accepted by this object.
     */
    private volatile long last = -1;

    /**
     * Link to context that lays under that abstraction.
     */
    @Getter
    private final Context context;

    /**
     * Send message only if {@link Message#getId()} bigger then {@link #last}.
     * <br> If requirements met then messages sent and {@link #last} updates to {@link Message#getId()}.
     *
     * @param messages message to send.
     */
    void accept(Message messages) {

        long messagesId = Objects.requireNonNull(messages.getId());

        if (messagesId > last) {
            last = messagesId;
            context.accept(messages);
        }
    }

    /**
     * Delegate method for {@link Context#isActive()}.
     */
    boolean isActive() {
        return context.isActive();
    }

    /**
     * Reset {@link #last} field.
     */
    void reset() {
        this.last = -1;
    }
}
