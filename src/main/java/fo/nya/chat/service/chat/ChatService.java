package fo.nya.chat.service.chat;

import fo.nya.chat.Context;
import fo.nya.chat.entity.Message;
import fo.nya.chat.entity.User;
import fo.nya.chat.repo.ChatRepo;
import fo.nya.chat.repo.MessageRepo;
import fo.nya.chat.service.UserService;
import fo.nya.chat.util.ReentrantLocksPool;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;

/**
 * Service that encapsulates common operation over chats entity and interaction with chats.
 */
public class ChatService {

    /**
     * Simple lookup table for quick access to {@code chatId} for specific {@code userId}.
     */
    private final Map<Long, Long> userIdToChatIdLookup = new ConcurrentHashMap<>();

    /**
     * Collections of currently active channels.
     * <br>
     * Chats active until there is some members with active connection or if last activity was less than a 'few' minute ago.
     */
    private final ConcurrentHashMap<Long, Chat> activeChats = new ConcurrentHashMap<>();

    /**
     * 'Active' abstractions for sending messages to a specific user.
     */
    private final Map<Long, Set<MessageSender>> messageSenders = new ConcurrentHashMap<>();

    /**
     * Lock pool of {@link #join(long, long)} and {@link #leave(long)} operation.
     */
    // random not small, not big prime number
    private final ReentrantLocksPool joinLeaveLockPool = new ReentrantLocksPool(23);

    /**
     * Queue of {@code chatId} that must be updated as soon as possible.
     */
    private final ArrayBlockingQueue<Long> chatUpdateQueue;

    /**
     * A queue of {@link} messages that will be broadcast as soon as possible.
     */
    private final ArrayBlockingQueue<Message> messageQueue;

    /**
     * Link to {@link UserService}, required update {@link User#getLatestChatId()}.
     */
    private final UserService users;

    /**
     * Link to chat repository.
     */
    private final ChatRepo chatRepo;

    /**
     * Link to message repository.
     */
    private final MessageRepo messageRepo;

    /**
     * Maximum amount of messages in chat backlog.
     */
    private final int chatBacklogSize;

    /**
     * Maximum amount of users per one chat.
     */
    private final int maximumAmountOfUsersPerChat;

    /**
     * Creates new ChatService. Also creates {@code chatUpdateThreadAmount} amount of {@link ChatUpdateThread} and {@code messageBroadcastThreadAmount} amount of {@link MessageBroadcastThread}.
     *
     * @param chatUpdateThreadAmount       amount of threads that works on chat update.
     * @param messageBroadcastThreadAmount amount of threads that works on async message store and delivery into chat.
     * @param chatBacklogSize              maximum amount of messages in chat backlog.
     * @param updateQueueSize              size of chat update queue.
     * @param messageQueueSize             maximum size of queue of async messages.
     * @param maximumAmountOfUsersPerChat  maximum amount of users per chat.
     * @param users                        link to {@link UserService}, required update {@link User#getLatestChatId()}.
     * @param chatRepo                     link to a Chat Repository.
     * @param messageRepo                  link to message repository.
     */
    public ChatService(int chatUpdateThreadAmount, int messageBroadcastThreadAmount, int chatBacklogSize, int updateQueueSize, int messageQueueSize, int maximumAmountOfUsersPerChat, UserService users, ChatRepo chatRepo, MessageRepo messageRepo) {

        this.users = users;
        this.chatRepo = chatRepo;
        this.messageRepo = messageRepo;
        this.chatBacklogSize = chatBacklogSize;
        this.maximumAmountOfUsersPerChat = maximumAmountOfUsersPerChat;
        this.chatUpdateQueue = new ArrayBlockingQueue<>(updateQueueSize);
        this.messageQueue = new ArrayBlockingQueue<>(messageQueueSize);

        for (int i = 0; i < chatUpdateThreadAmount; i++) {
            new ChatUpdateThread(this, chatUpdateQueue).start();
        }

        for (int i = 0; i < messageBroadcastThreadAmount; i++) {
            new MessageBroadcastThread(this, messageQueue).start();
        }
    }

    /**
     * Methods adds context into collection of 'income messages listeners'.
     *
     * @param context context to add.
     */
    public void addContext(Context context) {

        // After re-login, we need to clean up the old links.
        // Without some synchronization this system may not be reliable.

        // 'synchronized' may be not the best solution, but it works, and it's simple.
        // If same context decided to 'login' twice then there no chance for race.
        // In most cases that will work without blocking.
        synchronized (context) {

            for (Set<MessageSender> value : messageSenders.values()) {
                value.removeIf(next -> next.getContext() == context);
            }

            messageSenders.computeIfAbsent(context.getUserId(), __ -> ConcurrentHashMap.newKeySet())
                    .add(new MessageSender(context));
        }


        // Update in case user already joined in chat from other connection.

        Long chatId = userIdToChatIdLookup.get(context.getUserId());

        if (chatId != null) requestUpdate(chatId);
    }

    /**
     * Internal method for access to in memory object that represents chat.
     *
     * @param chatId {@code id} of a chat.
     * @param init   {@code true} if chat must be loaded and initialized event if not in memory.
     * @return If {@code init == true} then always return chat object or else return chat object only if it already in {@link #activeChats}.
     */
    private Chat getChat(long chatId, boolean init) {
        return init
                ? activeChats.computeIfAbsent(chatId, id -> {
                    Chat chat = new Chat();
                    chat.getBacklog().addAll(messageRepo.getLatest(id, chatBacklogSize));
                    chat.getLastActivity().accumulate(System.currentTimeMillis());

                    return chat;
                }
        ) : activeChats.get(chatId);
    }

    /**
     * Internal method that tries add {@code chatId} into chat update queue if it already not there.
     *
     * @param chatId chat {@code id} to offer into queue.
     * @see #chatUpdateQueue
     */
    private void requestUpdate(long chatId) {
        if (!chatUpdateQueue.contains(chatId)) {
            chatUpdateQueue.offer(chatId);
        }
    }

    /**
     * The method puts message in queue for {@link #broadcast(Message)} method.
     *
     * @param message message to send. {@link Message#getChatId()} must be not null.
     * @throws RuntimeException Because of {@link ArrayBlockingQueue#put(Object)}
     * @implSpec In most cases, a method should run without (long) locks and run in a predictable average time.
     */
    public void broadcastAsync(Message message) {
        try {
            messageQueue.put(message);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The method starts sending messages to all users in the specific channel.
     *
     * @param message message to send. {@link Message#getChatId()} must be not null.
     * @implNote Method will update {@link Chat#getLastActivity()}.
     */
    public void broadcast(Message message) {

        long chatId = Objects.requireNonNull(message.getChatId());

        message = messageRepo.save(message);

        Chat chat = getChat(chatId, true);
        chat.getBacklog().add(message);
        chat.getLastActivity().accumulate(System.currentTimeMillis());

        requestUpdate(chatId);

    }

    /**
     * Method returns {@code chatId} for specific name. If there is no such chat then method creates new chat with that name and returns fresh {@code id}.
     *
     * @param chatName name of chat.
     * @return {@code id} of a chat with name {@code chatName}.
     */
    public long getOrCreateChatIdForName(String chatName) {
        Long chatIdByName = chatRepo.getIdByName(chatName);

        return chatIdByName == null
                ? chatRepo.createWithName(chatName)
                : chatIdByName;
    }

    /**
     * Method returns name of a chat with defined {@code id}.
     *
     * @param chatId {@code id} of a chat.
     * @return name of a chat with {@code id}, {@code null} if chat with {@code id} isn't exist.
     */
    public String getChatName(long chatId) {
        return chatRepo.getNameById(chatId);
    }

    /**
     * Method returns {@code id} of all currently active chats.
     *
     * @return unmodifiable set of {@code id} of all active chats.
     * @see #activeChats
     */
    public Set<Long> getActiveChats() {
        return Collections.unmodifiableSet(activeChats.keySet());
    }

    /**
     * Method returns members of a chat with specified {@code id}.
     *
     * @param chatId {@code id} of a chat.
     * @return set of members in that chat. Method always returns at least empty set.
     */
    public Set<Long> getMembersOfChat(long chatId) {
        Chat chat = activeChats.get(chatId);
        if (chat == null) return Set.of();
        else return new HashSet<>(chat.getMembers());
    }

    /**
     * Method returns current {@code chatId} for specific user.
     *
     * @param userId {@code id} of a user.
     * @return current {@code chatId} for specific user. May be empty.
     */
    public Optional<Long> getChatIdForUser(long userId) {
        return Optional.ofNullable(userIdToChatIdLookup.get(userId));
    }

    /**
     * Method tries update chat with specified {@code chatId}.
     * <br> Update will happen only chat is not already in update and chat is active.
     * <br> Updating involves sending messages and cleaning up backlog.
     *
     * @param chatId {@code id} of a chat to update.
     * @return {@code true} if update happened or else {@code false}.
     * @see #activeChats
     */
    public boolean tryUpdate(long chatId) {

        Chat chat = getChat(chatId, false);

        if (chat == null) return false;

        ReentrantLock lock = chat.getLockForUpdate();

        if (!lock.tryLock()) return false;

        try {
            Queue<Message> backlog = chat.getBacklog();

            int overflow = Math.max(0, backlog.size() - chatBacklogSize);

            Iterator<Message> iterator = backlog.iterator();

            // Every message will be sent at least one time before remove.
            while (iterator.hasNext()) {

                Message next = iterator.next();

                if (overflow > 0) {
                    overflow--;
                    iterator.remove();
                }

                for (Long member : chat.getMembers()) {
                    Set<MessageSender> set = messageSenders.get(member);

                    if (set == null) continue;

                    for (MessageSender acceptor : set) {
                        acceptor.accept(next);
                    }
                }
            }

        } finally {
            lock.unlock();
        }

        return true;

    }

    /**
     * Method tries adds user into specified chat.
     *
     * @param chatId {@code id} of a chat.
     * @param userId {@code id} of a user.
     * @implNote Method will update {@link User#getLatestChatId()} and {@link Chat#getLastActivity()}.
     */
    public void join(long chatId, long userId) {

        ReentrantLock lock = joinLeaveLockPool.get(Long.hashCode(userId));

        lock.lock();
        try {

            if (Objects.equals(getChatIdForUser(userId).orElse(null), chatId)) {
                return;
            }

            leave(userId);

            Chat chat = getChat(chatId, true);

            // Safe size update.
            AtomicInteger size = chat.getSize();
            while (true) {

                int predicted = size.get();

                if (predicted >= maximumAmountOfUsersPerChat) {
                    User user = users.getUserById(userId);
                    user.setLatestChatId(null);
                    users.save(user);
                    throw new RuntimeException("Channel '" + getChatName(chatId) + "' has the maximum number of people. Please join another one.");
                }

                if (size.compareAndSet(predicted, predicted + 1)) {
                    break;
                }
            }

            // Add user to members collection
            chat.getMembers().add(userId);

            // Update the latest chat id.
            User user = users.getUserById(userId);
            user.setLatestChatId(chatId);
            users.save(user);

            // Reset message senders to new chat.
            Set<MessageSender> senders = messageSenders.get(userId);
            if (senders != null) {
                for (MessageSender sender : senders) {
                    sender.reset();
                }
            }

            // Update lookup table.
            userIdToChatIdLookup.put(userId, chatId);

            // Update the latest chat activity.
            chat.getLastActivity().accumulate(System.currentTimeMillis());

            broadcastAsync(Message.of(chatId, null, "User '" + user.getName() + "' joined."));
            requestUpdate(chatId);

        } finally {
            lock.unlock();
        }

    }

    /**
     * Method removes user from his current chat. Or does nothing if user is not in a chat.
     *
     * @param userId {@code id} of a user.
     * @implNote Method will update {@link User#getLatestChatId()} and {@link Chat#getLastActivity()}.
     */
    public void leave(long userId) {

        ReentrantLock lock = joinLeaveLockPool.get(Long.hashCode(userId));

        lock.lock();
        try {
            Long chatId = userIdToChatIdLookup.remove(userId);

            if (chatId == null) return;

            Chat chat = getChat(chatId, false);

            if (chat == null) return;

            if (chat.getMembers().remove(userId)) {
                chat.getSize().decrementAndGet();

                // Update the latest chat id.
                User user = users.getUserById(userId);
                user.setLatestChatId(null);
                users.save(user);

                broadcastAsync(Message.of(chatId, null, "User '" + user.getName() + "' left."));
                requestUpdate(chatId);

            }

        } finally {
            lock.unlock();
        }
    }

    /**
     * Method cleans closed context, empty channels, etc. Must be called regularly.
     */
    public void clean() {

        // Remove closed contexts.
        for (Set<MessageSender> value : messageSenders.values()) {
            value.removeIf(Predicate.not(MessageSender::isActive));
        }

        // Remove users without contexts.
        messageSenders.entrySet().removeIf(e -> e.getValue().isEmpty());

        // Remove users from lookup table and chat if they are not connected.
        var iterator = userIdToChatIdLookup.entrySet().iterator();

        while (iterator.hasNext()) {
            var userIdAndChatId = iterator.next();
            long userId = Objects.requireNonNull(userIdAndChatId.getKey());
            long chatId = Objects.requireNonNull(userIdAndChatId.getValue());

            if (messageSenders.containsKey(userId)) continue;


            ReentrantLock lock = joinLeaveLockPool.get(Long.hashCode(userId));

            lock.lock();
            try {
                iterator.remove();

                Chat chat = activeChats.get(chatId);
                if (chat == null) continue;

                if (chat.getMembers().remove(userId)) {
                    chat.getSize().decrementAndGet();
                    broadcastAsync(Message.of(chatId, null, "User '" + Optional.ofNullable(users.getUserById(userId)).map(User::getName).orElse("[id:" + userId + "]") + "' was removed due to inactivity."));
                }

            } finally {
                lock.unlock();
            }

        }

        // Remove chat without any activity.
        activeChats.values().removeIf(chat -> (TimeUnit.MILLISECONDS.toMinutes(Math.max(0, System.currentTimeMillis() - chat.getLastActivity().get())) > 3)
                                              && chat.getSize().compareAndSet(0, maximumAmountOfUsersPerChat));
    }
}

