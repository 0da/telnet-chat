package fo.nya.chat.service.chat;

import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Daemon thread that asynchronously performs chat updates.
 *
 * @see ChatService#tryUpdate(long)
 */
class ChatUpdateThread extends Thread {

    /**
     * Chat service for access to {@link ChatService#tryUpdate(long)}.
     */
    private final ChatService chats;

    /**
     * Queue of a chat ids that require update.
     */
    private final ArrayBlockingQueue<Long> queue;

    /**
     * Creates new daemon thread with name 'Chat Update Thread'.
     *
     * @param chats chat service for access to {@link ChatService#tryUpdate(long)}.
     * @param queue queue of a chat ids that require update.
     */
    public ChatUpdateThread(ChatService chats, ArrayBlockingQueue<Long> queue) {
        super("Chat Update Thread");
        setDaemon(true);

        this.chats = chats;
        this.queue = queue;
    }

    /**
     * Method takes an {@code id} from the queue and tries to update it,
     * if it fails, the thread tries to put the {@code id} back into the queue,
     * if it failed to do it instantly, then this {@code id} is skipped.
     *
     * @see ChatService#tryUpdate(long)
     * @see ArrayBlockingQueue#take()
     * @see ArrayBlockingQueue#offer(Object)
     */
    @Override public void run() {
        while (!Thread.currentThread().isInterrupted()) {

            try {

                long poll = Objects.requireNonNull(queue.take());

                if (!chats.tryUpdate(poll)) {
                    queue.offer(poll);
                }

            } catch (InterruptedException e) {
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
