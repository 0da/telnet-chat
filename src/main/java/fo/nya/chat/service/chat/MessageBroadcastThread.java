package fo.nya.chat.service.chat;

import fo.nya.chat.entity.Message;

import java.util.Objects;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Daemon thread that asynchronously performs message broadcasting.
 *
 * @see ChatService#broadcast(Message)
 */
class MessageBroadcastThread extends Thread {

    /**
     * Chat service for access to {@link ChatService#broadcast(Message)}.
     */
    private final ChatService chats;

    /**
     * Queue of a messages that require broadcast.
     */
    private final ArrayBlockingQueue<Message> queue;

    /**
     * Creates new daemon thread with name 'Message Broadcast Thread'.
     *
     * @param chats chat service for access to {@link ChatService#broadcast(Message)}.
     * @param queue queue of a messages that require broadcast.
     */
    public MessageBroadcastThread(ChatService chats, ArrayBlockingQueue<Message> queue) {
        super("Message Broadcast Thread");
        setDaemon(true);

        this.chats = chats;
        this.queue = queue;
    }

    /**
     * Method takes an {@code id} from the queue and calls {@link ChatService#broadcast(Message)} for that message.
     *
     * @see ChatService#broadcast(Message)
     * @see ArrayBlockingQueue#take()
     */
    @Override public void run() {
        while (!Thread.currentThread().isInterrupted()) {

            try {

                var poll = Objects.requireNonNull(queue.take());
                chats.broadcast(poll);

            } catch (InterruptedException e) {
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }
}
