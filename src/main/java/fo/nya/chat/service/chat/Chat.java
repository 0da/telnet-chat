package fo.nya.chat.service.chat;

import fo.nya.chat.entity.Message;
import lombok.Value;

import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAccumulator;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Object represents active chat.
 */
@Value class Chat {

    /**
     * Lock object that used for chat update.
     *
     * @see ChatService#tryUpdate(long)
     */
    ReentrantLock lockForUpdate = new ReentrantLock();

    /**
     * List of latest messages.
     */
    // as concurrent linked list
    Queue<Message> backlog = new ConcurrentLinkedQueue<>();

    /**
     * Current members in that chat.
     */
    Set<Long> members = ConcurrentHashMap.newKeySet();

    /**
     * Current members amount.
     */
    AtomicInteger size = new AtomicInteger();

    /**
     * Date of last chat activity in milliseconds.
     */
    LongAccumulator lastActivity = new LongAccumulator(Long::max, 0);
}
