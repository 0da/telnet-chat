package fo.nya.chat.service;

import fo.nya.chat.entity.User;
import fo.nya.chat.repo.UserRepo;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

/**
 * Service that encapsulates common operation over user entity.
 */
@RequiredArgsConstructor
public class UserService {

    /**
     * Link to user repository.
     */
    private final UserRepo repo;

    /**
     * Authenticates into existing user account or creates new one of there is no user with such {@code name}
     *
     * @param name username of user to authenticate.
     * @param pass password belonging to this {@code username}.
     * @return user object corresponding to the submitted credentials.
     */
    public User loginOrCreateNewUser(String name, String pass) {

        User user = repo.getByName(name);

        // Concurrency doesn't matter, because it's responsibility of persistence to keep unique names.
        if (user == null) {
            user = new User();
            user.setPass(pass);
            user.setName(name);
            user = repo.save(user);

            return user;
        }

        if (Objects.equals(user.getPass(), pass)) return user;

        throw new RuntimeException("Illegal credential. Try again.");
    }

    /**
     * Method returns user corresponding to some ID.
     *
     * @param id ID of a user to return.
     * @return User with provided id, result may be null, if no user with such ID.
     */
    public User getUserById(long id) {
        return repo.getById(id);
    }

    /**
     * Method saves provided object and returns new actual object that should be used.
     *
     * @param user entity to save. Must be not null.
     * @return new entity with more actual data based on persistence.
     */
    public User save(User user) {
        return repo.save(user);
    }
}
