package fo.nya.chat.repo;

import fo.nya.chat.entity.User;

/**
 * Repository for {@link User} entity.
 */
public interface UserRepo {

    /**
     * Method returns user corresponding to some {@code id}.
     *
     * @param id ID of a user to return.
     * @return User with provided {@code id}, result may be null, if no user with such  {@code id}.
     * @see User#getId();
     */
    User getById(long id);

    /**
     * Method returns user corresponding to some {@code name}.
     *
     * @param name name of a user to return.
     * @return User with provided {@code name}, result may be null, if no user with such {@code name}.
     */
    User getByName(String name);

    /**
     * Method saves provided object into persistence and returns new actual object that should be used.
     *
     * @param user entity to save. Must be not null.
     * @return new entity with more actual data based on persistence.
     */
    User save(User user);
}
