package fo.nya.chat.repo;

/**
 * Repository for chat entity.
 */
public interface ChatRepo {

    /**
     * Method returns {@code id} of chat with corresponding {@code name}.
     *
     * @param name name of a chat.
     * @return {@code id} of a chat with {@code name} or {@code null} if such chat isn't exist.
     */
    Long getIdByName(String name);

    /**
     * Method returns {@code name} of chat with corresponding {@code id}.
     *
     * @param id id of a chat.
     * @return {@code name} of a chat with {@code id} or {@code null} if such chat isn't exist.
     */
    String getNameById(long id);

    /**
     * Method creates new chat with {@code name}.
     *
     * @param name required name of a chat. Must be not null.
     * @return {@code id} of freshly created chat.
     */
    long createWithName(String name);
}
