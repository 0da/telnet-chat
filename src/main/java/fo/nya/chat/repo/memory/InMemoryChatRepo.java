package fo.nya.chat.repo.memory;

import fo.nya.chat.repo.ChatRepo;
import fo.nya.chat.util.ReentrantLocksPool;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 'In Memory' implementation of {@link ChatRepo}.
 */
public class InMemoryChatRepo implements ChatRepo {

    /**
     * Simple imitation of unique ids.
     */
    private final static AtomicLong IDS = new AtomicLong(System.currentTimeMillis());

    /**
     * Actual 'storage'.
     */
    private final ConcurrentHashMap<Long, String> chats = new ConcurrentHashMap<>();

    /**
     * Simple 'lock pool' for flimsy operations.
     */
    private final ReentrantLocksPool locks = new ReentrantLocksPool(13);

    /**
     * {@inheritDoc}
     */
    @Override public Long getIdByName(String name) {

        for (var entry : chats.entrySet()) {
            if (Objects.equals(name, entry.getValue())) {
                return entry.getKey();
            }
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override public String getNameById(long chatId) {
        return chats.get(chatId);
    }

    /**
     * {@inheritDoc}
     *
     * @throws RuntimeException in case if chat with same name already exist.
     */
    @Override public long createWithName(String name) {

        Objects.requireNonNull(name);

        // Lock by that name.
        ReentrantLock lock = locks.get(name.hashCode());

        lock.lock();
        try {

            Long idByName = getIdByName(name);

            if (idByName != null) {
                throw new RuntimeException("Chat with same name already exist.");
            }

            long id = IDS.incrementAndGet();

            chats.put(id, name);

            return id;

        } finally {
            lock.unlock();
        }

    }
}
