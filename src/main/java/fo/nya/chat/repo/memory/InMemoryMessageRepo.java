package fo.nya.chat.repo.memory;

import fo.nya.chat.entity.Message;
import fo.nya.chat.repo.MessageRepo;

import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 'In Memory' implementation of {@link MessageRepo}.
 *
 * @see #STORE_LIMIT
 */
public class InMemoryMessageRepo implements MessageRepo {

    /**
     * Simple imitation of unique ids.
     */
    private final static AtomicLong IDS = new AtomicLong(System.currentTimeMillis());

    /**
     * Maximum amount of stored messages. Just in case.
     */
    private final int STORE_LIMIT = 1000;

    /**
     * Actual 'storage'.
     */
    // as concurrent linked list
    private final ConcurrentLinkedQueue<Message> messages = new ConcurrentLinkedQueue<>();

    /**
     * {@inheritDoc}
     * <br> This implementation saves every message as new without update old one.
     */
    @Override public Message save(Message message) {

        // copy
        message = new Message(message);

        message.setId(IDS.incrementAndGet());
        messages.add(message);

        int overflow = messages.size() - STORE_LIMIT;

        for (int i = 0; i < overflow; i++) {
            messages.poll();
        }

        return new Message(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override public List<Message> getLatest(long chatId, int n) {
        return messages.stream()
                .filter(message -> message.getChatId() == chatId)
                .sorted(Comparator.comparingLong(Message::getId).reversed())
                .limit(n)
                .sorted(Comparator.comparingLong(Message::getId))
                .toList();
    }
}
