package fo.nya.chat.repo.memory;

import fo.nya.chat.entity.User;
import fo.nya.chat.repo.UserRepo;
import fo.nya.chat.util.ReentrantLocksPool;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 'In Memory' implementation of {@link UserRepo}.
 */
public class InMemoryUserRepo implements UserRepo {

    /**
     * Simple imitation of unique ids.
     */
    private final static AtomicLong IDS = new AtomicLong(System.currentTimeMillis());

    /**
     * Actual 'storage'.
     */
    private final ConcurrentHashMap<Long, User> users = new ConcurrentHashMap<>();

    /**
     * Simple 'lock pool' for flimsy operations.
     */
    private final ReentrantLocksPool locks = new ReentrantLocksPool(13);

    /**
     * {@inheritDoc}
     */
    @Override public User getById(long id) {
        User original = users.get(id);
        if (original == null) return null;
        return new User(original);
    }

    /**
     * {@inheritDoc}
     */
    @Override public User getByName(String name) {
        for (User value : users.values()) {
            if (Objects.equals(value.getName(), name)) {
                return new User(value);
            }
        }
        return null;
    }

    /**
     * {@inheritDoc}
     *
     * @throws RuntimeException in case if users with same name but different id already exist.
     */
    @Override public User save(User user) {
        Objects.requireNonNull(user);

        User copy = new User(user);

        if (copy.getId() == null) {
            copy.setId(IDS.incrementAndGet());
        }

        String name = copy.getName();

        int lockIndex = (name == null ? 0 : name.hashCode());

        // Lock by name.
        ReentrantLock lock = locks.get(lockIndex);

        lock.lock();
        try {

            User byName = getByName(copy.getName());

            if (byName == null || Objects.equals(byName.getId(), copy.getId())) {
                users.put(copy.getId(), copy);
            } else {
                throw new RuntimeException("User with same name already exist.");
            }

        } finally {
            lock.unlock();
        }

        return new User(copy);
    }
}
