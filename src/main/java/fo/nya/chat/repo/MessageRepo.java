package fo.nya.chat.repo;

import fo.nya.chat.entity.Message;

import java.util.List;

/**
 * Repository for {@link Message} entity.
 */
public interface MessageRepo {

    /**
     * Method saves provided object into persistence and returns new actual object that should be used.
     *
     * @param message entity to save. Must be not null.
     * @return new entity with more actual data based on persistence.
     */
    Message save(Message message);

    /**
     * Returns {@code n} latest messages for specified {@code chatId}.
     *
     * @param chatId {@code id} of a chat.
     * @param n      maximum amount of messages to return.
     * @return collection that has no more than {@code n} messages sorted from oldest to newest.
     */
    List<Message> getLatest(long chatId, int n);
}
