package fo.nya.chat.util;

import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

/**
 * Simple lock pool.
 */
public class ReentrantLocksPool {

    /**
     * Actual locks.
     */
    private final ReentrantLock[] pool;

    /**
     * Creates new lock pool with {@code amount} locks.
     *
     * @param amount amounts of lock in that pool.
     */
    public ReentrantLocksPool(int amount) {
        this.pool = Stream.generate(ReentrantLock::new)
                .limit(amount)
                .toArray(ReentrantLock[]::new);
    }

    /**
     * Returns lock based on {@code salt}, for same {@code salt} always returned same lock.
     *
     * @param salt some integer that are always same for lockable object.
     * @return lock based on {@code salt}, for same {@code salt} always returned same lock.
     */
    public ReentrantLock get(int salt) {
        return pool[Math.abs(salt) % pool.length];
    }
}
